image: maven:3-jdk-11

variables:
  MAVEN_CLI_OPTS: "-s .ci/settings.xml --batch-mode"
  MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository"
  DS_DEFAULT_ANALYZERS: "gemnasium-maven"

cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - .m2/repository/

stages:
  - test
  - deploy

test:jdk11:
  stage: test
  image: registry.gitlab.com/tinymediamanager/docker/jdk11
  except:
    - main@tinyMediaManager/tinyMediaManager
  only:
    - pushes
  script:
    - mvn $MAVEN_CLI_OPTS -U -DskipTests=false -DskipITs=true -Dmaven.test.failure.ignore=false clean compile test
  artifacts:
    expire_in: 1 days

test:jdk14:
  stage: test
  image: registry.gitlab.com/tinymediamanager/docker/jdk14
  except:
    - main@tinyMediaManager/tinyMediaManager
  only:
    - pushes
  script:
    - mvn $MAVEN_CLI_OPTS -U -DskipTests=false -DskipITs=true -Dmaven.test.failure.ignore=false clean compile test
  artifacts:
    expire_in: 1 days

# use dependency scanning (only on QA)
include:
  template: Dependency-Scanning.gitlab-ci.yml

gemnasium-maven-dependency_scanning:
  variables:
    MAVEN_CLI_OPTS: "-DskipTests --batch-mode"
  rules:
    - if: '$QA_BUILD == "true"'
  artifacts:
    reports:
      dependency_scanning: gl-dependency-scanning-report.json

# sonarqube analysis
sonarqube-qa:
  stage: test
  only:
    variables:
      - $QA_BUILD == "true"
  except:
    - pushes
  allow_failure: true
  script:
    - mvn $MAVEN_CLI_OPTS clean compile
    - mvn $MAVEN_CLI_OPTS sonar:sonar -Dsonar.projectKey=org.tinymediamanager:tinyMediaManager -Dsonar.organization=tinymediamanager -Dsonar.host.url=https://sonarcloud.io -Dsonar.login=${SONARCLOUD_API_TOKEN}
  artifacts:
    expire_in: 1 days

deploy:nightly:
  stage: deploy
  image: maven:3-jdk-11                                # stay with stretch
  environment:
    name: nightly
    url: https://nightly.tinymediamanager.org
  only:
    variables:
      - $NIGHTLY_BUILD == "true"
  except:
    - pushes                                          # do not run packaging on push-builds
  script:
    # update package sources and install ant, 32 bit libs, git and lftp
    - dpkg --add-architecture i386 && apt-get update && apt-get install -y --no-install-recommends ant libstdc++6:i386 libgcc1:i386 zlib1g:i386 libncurses5:i386 git lftp curl
    # generate changelog.txt for nightly builds
    - ./generate_changelog.sh
    # package
    - mvn $MAVEN_CLI_OPTS -P gitlab-ci -P dist -DbuildNumber=${CI_COMMIT_SHA:0:8} -Dgetdown=getdown-nightly.txt -Dnightly=true -Dthread_pool_size=1 clean compile package

    # write the job ID to the repo
    - curl -X PUT -H "PRIVATE-TOKEN:${TMMBUILD_AUTH_TOKEN}" -H "Content-Type:application/json" --data "{\"branch\":\"master\", \"author_email\":\"tinymediamanager@tinymediamanager.org\", \"author_name\":\"tinyMediaManager\", \"content\":\"${CI_JOB_ID}\", \"commit_message\":\"new build\"}" https://gitlab.com/api/v4/projects/7895208/repository/files/v4_job_id.txt

    # trigger the deployment on nightly.tinymediamanager.org
    - curl -X POST -F "token=${NIGHTLY_PIPELINE_TOKEN}" -F "ref=master" https://gitlab.com/api/v4/projects/7895208/trigger/pipeline

  artifacts:
    paths:
      - build/
      - dist/
    expire_in: 1 week

deploy:prerel:
  stage: deploy
  image: maven:3-jdk-11  # stay with stretch
  environment:
    name: prerelease
    url: https://prerelease.tinymediamanager.org
  only:
    - main@tinyMediaManager/tinyMediaManager
  when: manual
  script:
    # update package sources and install ant + 32 bit libs
    - dpkg --add-architecture i386 && apt-get update && apt-get install -y --no-install-recommends ant libstdc++6:i386 libgcc1:i386 zlib1g:i386 libncurses5:i386 lftp curl
    # package
    - mvn $MAVEN_CLI_OPTS -P gitlab-ci -DbuildNumber=${CI_COMMIT_SHA:0:8} -Dgetdown=getdown-prerelease.txt -Dprerelease=true -Dthread_pool_size=1 clean compile package

    # write the job ID to the repo
    - curl -X PUT -H "PRIVATE-TOKEN:${TMMBUILD_AUTH_TOKEN}" -H "Content-Type:application/json" --data "{\"branch\":\"master\", \"author_email\":\"tinymediamanager@tinymediamanager.org\", \"author_name\":\"tinyMediaManager\", \"content\":\"${CI_JOB_ID}\", \"commit_message\":\"new build\"}" https://gitlab.com/api/v4/projects/10869644/repository/files/v4_job_id.txt

    # trigger the deployment on prerelease.tinymediamanager.org (new HP)
    - curl -X POST -F "token=${PREREL_PIPELINE_TOKEN}" -F "ref=master" https://gitlab.com/api/v4/projects/10869644/trigger/pipeline

  artifacts:
    paths:
      - build/
      - dist/
    expire_in: 1 month

deploy:RELEASE:
  stage: deploy
  image: maven:3-jdk-11      # use stretch
  environment:
    name: release
    url: https://release.tinymediamanager.org
  when: manual
  only:
    - main@tinyMediaManager/tinyMediaManager
  script:
    # update package sources and install ant + 32 bit libs
    - dpkg --add-architecture i386 && apt-get update && apt-get install -y --no-install-recommends ant libstdc++6:i386 libgcc1:i386 zlib1g:i386 libncurses5:i386 lftp curl

    ##
    ## Install ssh-agent if not already installed, it is required by Docker.
    ## (change apt-get to yum if you use an RPM-based image)
    ##
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'

    ##
    ## Run ssh-agent (inside the build environment)
    ##
    - eval $(ssh-agent -s)

    ##
    ## Add the SSH key stored in SSH_PRIVATE_KEY variable to the agent store
    ## We're using tr to fix line endings which makes ed25519 keys work
    ## without extra base64 encoding.
    ## https://gitlab.com/gitlab-examples/ssh-private-key/issues/1#note_48526556
    ##
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null

    ##
    ## Create the SSH directory and give it the right permissions
    ##
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - ssh-keyscan gitlab.com >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts

    ##
    ## set gpg key
    ##
    - echo "${GPG_PRIVATE_KEY}" | gpg --batch --import
    - gpg --list-keys

    ##
    ## configure git for version push
    ##
    - git config --global user.name ${SSH_USER_NAME}
    - git config --global user.email ${SSH_USER_EMAIL}
    - git checkout -f ${CI_COMMIT_REF_NAME}
    - git remote set-url origin git@gitlab.com:tinyMediaManager/${CI_PROJECT_NAME}.git

    ##
    ## perform the release
    ##
    - mvn $MAVEN_CLI_OPTS -P gitlab-ci -DbuildNumber=${CI_COMMIT_SHA:0:8} -Dgetdown=getdown.txt -Dthread_pool_size=1 release:prepare release:perform -Dresume=false -DautoVersionSubmodules=true -DdryRun=false -Dmaven.test.skip=true -DskipITs -DscmCommentPrefix="[ci skip]"

    # trigger the deployment on release.tinymediamanager.org (new HP)
    - curl -X POST -F "token=${RELEASE_PIPELINE_TOKEN}" -F "ref=master" -F "variables[JOB_ID]=${CI_JOB_ID}" https://gitlab.com/api/v4/projects/10869704/trigger/pipeline

  artifacts:
    paths:
      - build/
      - dist/
